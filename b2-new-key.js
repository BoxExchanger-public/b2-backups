const axios = require("axios");
const promptly = require("promptly");

(async () => {
  const username = await promptly.prompt('B2 application key ID: ', {});
  const password = await promptly.prompt('B2 application key: ', {});
  const buckets = await promptly.prompt('B2 buckets ID: ', {});

  console.log("Authorization...");
  const authed = await axios({
    method: "get",
    url: "https://api.backblazeb2.com/b2api/v2/b2_authorize_account",
    auth: {
      username: username,
      password: password
    }
  })
    .then(r => r.data);

  if (authed.allowed.capabilities.indexOf("writeKeys") === -1) {
    return console.error("ERROR: You must provide MasterKey for generate new kay (or key with capabilities writeKeys)")
  }
  console.log("Authorization [OK]");
  console.log("Creating key...");
  const createdAppKey = await axios({
    method: "post",
    url: authed.apiUrl + "/b2api/v2/b2_create_key",
    headers: {
      "Authorization": authed.authorizationToken
    },
    data: {
      accountId: authed.accountId,
      keyName: "only-w-" + Date.now(),
      bucketId: buckets,
      capabilities: [
        'writeFiles',
        'listFiles',
        'listBuckets'
      ]
    }
  })
    .then(r => r.data)
    .catch(error => {
      console.error("Error: Create app key:", error.message);
      if (error && error.response && error.response.data && error.response.data.message)
        console.error("\t->", error.response.data.message);
      return;
    });
  if (!createdAppKey) return;
  console.log(`
  +++++++++++++++++++++ [Created key] +++++++++++++++++++++
  \tAccountId:    ${createdAppKey.accountId}
  \tApp Key Id:   ${createdAppKey.applicationKeyId}
  \tApp Key:      ${createdAppKey.applicationKey}
  =========================================================
  `)
})()