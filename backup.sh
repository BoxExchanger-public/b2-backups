#!/bin/bash
# BoxExchanger backup script.
# Version: 2022.09.06-2

#
# -- Configuration --

# rclone remote name.
RCLONE_REMOTE="b2"
# b2 bucket name.
B2_BUCKET="name-project"
# Telegram Bot Token
BOT_TOKEN="1234567890:ABCDEF1234567890ABCDEF1234567890ABC"
# (list) of Telegram Chat IDs.
# e.g: CHAT_IDS=("1234567890" "0987654321")
CHAT_IDS=("1234567890")

#
# -- Init Default Vars --

# Set Backup script startup time
ENTRY_TIME=$(date +%Y-%m-%d-%H-%M-%S)
# Hostname
HOSTNAME=$(hostname)
# Set the path to the BoxExchanger directory.
BOXEXCHANGER_API_PATH="/var/www/exchanger-api"
# Set the path to the BoxExchanger backup directory.
BOXEXCHANGER_BACKUP_TMP_PATH="/tmp/backup"
# Fetch MongoDB uri from config file.
MONGODB_URI=$(grep -oP '(?<="uri":."|"uri":").*(?=")' $BOXEXCHANGER_API_PATH/config/app_config.json)

ERR_MSG=""

trap 'abort' 0
set -e

#
# -- Init Helpers --

# Telegram Notification
sendTelegramMessage() {
    # Set the Telegram message.
    MESSAGE="$1"
    # Toggle to enable/disable Telegram notifications.
    SILENT="$2"
    # Send the Telegram message.
    for CHAT_ID in "${CHAT_IDS[@]}"; do
        curl -s -X POST "https://api.telegram.org/bot$BOT_TOKEN/sendMessage" -d chat_id="$CHAT_ID" -d text="$MESSAGE" -d disable_notification="$SILENT" >/dev/null
    done
}
abort() {
    sendTelegramMessage "🔥 Backup on $(hostname) has crashed :(" false
    if [ -n "$ERR_MSG" ]; then
        sendTelegramMessage "❌ $ERR_MSG" "false"
    fi
    echo "An error occurred during backup. Exiting..." >&2
    exit 1
}

#
# -- Sanity Checks --

# Run as root.
if [ "$UID" -ne 0 ]; then
    ERR_MSG="[$HOSTNAME] Backup script must be run as root."
    echo "Must be root to run this script."
    exit $E_NOTROOT
fi

# Verify that rclone exists
if ! command -v rclone &> /dev/null; then
    ERR_MSG="[$HOSTNAME] rclone is not installed."
    echo "rclone is not installed. Please install it first."
    exit 1
fi

# verify that rclone remote exists
if ! rclone listremotes | grep -q "$RCLONE_REMOTE"; then
    ERR_MSG="[$HOSTNAME] rclone remote $RCLONE_REMOTE does not exist."
    echo "rclone remote $RCLONE_REMOTE does not exist. Please create it first."
    exit 1
fi

# Verify that b2 bucket and access rights
if ! rclone lsd $RCLONE_REMOTE:$B2_BUCKET >/dev/null; then
    ERR_MSG="[$HOSTNAME] b2 bucket $B2_BUCKET does not exist."
    echo "b2 bucket $B2_BUCKET does not exist or you don't have access to it. Please create it first."
    exit 1
fi

#
# -- Backup --

echo "[$ENTRY_TIME] Starting backup on $HOSTNAME..."

# init backup directories
mkdir -p $BOXEXCHANGER_BACKUP_TMP_PATH/$HOSTNAME

# backup mongodb
mongodump --uri $MONGODB_URI --gzip --archive=$BOXEXCHANGER_BACKUP_TMP_PATH/$HOSTNAME/mongodb.gz

# validate mongodb backup
if [ ! -f $BOXEXCHANGER_BACKUP_TMP_PATH/$HOSTNAME/mongodb.gz ]; then
    ERR_MSG="[$HOSTNAME] MongoDB backup failed. (Not Found)"
    echo "MongoDB backup failed."
    exit 1
fi

# Packup BoxExchanger Static and config
tar -czf $BOXEXCHANGER_BACKUP_TMP_PATH/$HOSTNAME/boxexchanger-public.tar.gz $BOXEXCHANGER_API_PATH/public
tar -czf $BOXEXCHANGER_BACKUP_TMP_PATH/$HOSTNAME/boxexchanger-config.tar.gz $BOXEXCHANGER_API_PATH/config

# Commence backup
rclone copy $BOXEXCHANGER_BACKUP_TMP_PATH/$HOSTNAME $RCLONE_REMOTE:$B2_BUCKET/$HOSTNAME

trap : 0 # reset trap
sendTelegramMessage "📦 Backup on $HOSTNAME completed successfully. \n $(du -sh $BOXEXCHANGER_BACKUP_TMP_PATH/$HOSTNAME) -> $RCLONE_REMOTE:$B2_BUCKET/$HOSTNAME" true
echo "Backup completed successfully."

# Cleanup
rm -rf $BOXEXCHANGER_BACKUP_TMP_PATH

exit 0
